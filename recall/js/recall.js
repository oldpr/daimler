$(document).ready(function(){
	 
    $(".valid").submit(function() {
        if ($(".valid .form-control").val() == ""){
            $(".valid .form-control").addClass("erro");
            return false;
        }else{
            $(".valid .form-control").removeClass("erro");
        }
        var url="";
        if($(this).hasClass("m2")){
            url="recallBackend/Conexion_ml.php";
        }else{
            url="recallBackend/query.php";
        }
      
        var chasis = $('#chasis').val();
        var tipo = $('#tipo').val();

        $.ajax({
                type: "POST",
                url: url,
                data: "tipo="+ tipo +"&codigo="+ chasis,
                beforeSend: function() {
                    $('.validarchasis').addClass("buscar");
                    $('.validarchasis').html("Buscando <img src='../img/loader.gif' />");
                 },
                success: function(msg){
                         $(".texto-inicio").hide();
                         $(".msj").hide();
                         $(".nchasis").html(chasis);
                        if(msg == "no"){
                            $('.validarchasis').addClass("fail");
                            $('.validarchasis').html("No encontrado");
                            $(".msjrechaza").fadeIn(1500);
                            
                        }else{
                            $('.validarchasis').addClass("done");
                            $('.validarchasis').html("Chasis encontrado");
                            $(".msjconfirma").fadeIn(1500);
                        }
                }
            });
            return false;
    });
    $("#chasis").focus(function(event) {
        reiniciar();
    });
   $("#chasis").keyup(function(){
       if($(this).val() == ""){
           reiniciar()
       }
    });
        
});

function reiniciar(){
    $('#chasis').val("");
    $('.validarchasis').removeClass("done fail buscar");
    $('.validarchasis').html("Validar chasis");
    $(".msj").hide();
    $(".texto-inicio").fadeIn(1500);
}