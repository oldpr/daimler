<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConcesionariosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->
            add('ciudad')
            ->add('nombre', 'text', array(
                'label'=>'Nombre',
                'required' => true,
                'label_attr'=> array('class'=>'col-md-2 control-label'),
                'attr'=> array('class'=> 'col-md-12 form-control'),
            ))
            ->add('descripcion','textarea', array(
                'label'=>'Descripción',
                'required' => false,
                'label_attr'=> array('class'=>'col-md-12 control-label'),
                'attr' => array('class' => 'form-control summernote')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Concesionarios'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_concesionarios';
    }


}
