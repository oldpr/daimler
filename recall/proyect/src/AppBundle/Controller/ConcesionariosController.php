<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concesionarios;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Concesionario controller.
 *
 */
class ConcesionariosController extends Controller
{
    /**
     * Lists all concesionario entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $concesionarios = $em->getRepository('AppBundle:Concesionarios')->findAll();

        return $this->render('concesionarios/index.html.twig', array(
            'concesionarios' => $concesionarios,
        ));
    }

    /**
     * Creates a new concesionario entity.
     *
     */
    public function newAction(Request $request, $id)
    {
        $concesionario = new Concesionarios();
        $ciudad = $this->getDoctrine()->getRepository('AppBundle:Ciudades')->find($id);
        $concesionario->setCiudad($ciudad);
        $form = $this->createForm('AppBundle\Form\ConcesionariosType', $concesionario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($concesionario);
            $em->flush();

            return $this->redirectToRoute('admin_concesionarios_edit', array('id' => $concesionario->getId()));
        }

        return $this->render('concesionarios/new.html.twig', array(
            'concesionario' => $concesionario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a concesionario entity.
     *
     */
    public function showAction(Concesionarios $concesionario)
    {
        $deleteForm = $this->createDeleteForm($concesionario);

        return $this->render('concesionarios/show.html.twig', array(
            'concesionario' => $concesionario,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing concesionario entity.
     *
     */
    public function editAction(Request $request, Concesionarios $concesionario)
    {
        $deleteForm = $this->createDeleteForm($concesionario);
        $editForm = $this->createForm('AppBundle\Form\ConcesionariosType', $concesionario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_concesionarios_edit', array('id' => $concesionario->getId()));
        }

        return $this->render('concesionarios/edit.html.twig', array(
            'concesionario' => $concesionario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a concesionario entity.
     *
     */
    public function deleteAction(Request $request, Concesionarios $concesionario)
    {
        $form = $this->createDeleteForm($concesionario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($concesionario);
            $em->flush();
        }

        return $this->redirectToRoute('admin_ciudades_index');
    }

    /**
     * Creates a form to delete a concesionario entity.
     *
     * @param Concesionarios $concesionario The concesionario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Concesionarios $concesionario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_concesionarios_delete', array('id' => $concesionario->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
