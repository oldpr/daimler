<?php 
include "include/header.php";
?>
<div class="inner clientes">
    <div class="tituloheader"><h1>Red de concesionarios</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera conce" style="background-image: url('img/concesionarios.jpg')"></div>
    <!--CONTENIDOS-->
    <div class="container content-interna">
        <div class="row">
            <!--MENU LATERAL -->
            <aside class="col-lg-3 col-md-3 col-sm-4 ">
                <nav class="menuLateral">
                    <ul>
                        <li><a data-id="#bogota">BOGOTÁ</a></li>
                        <li><a data-id="#barranquilla">BARRANQUILLA</a></li>
                        <li><a data-id="#boyaca">BOYACÁ</a></li>
                        <li><a data-id="#bucaramanga">BUCARAMANGA</a></li>
                        <li><a data-id="#cali">CALI</a></li>
                        <li><a data-id="#cucuta" >CÚCUTA</a></li>
                        <li><a data-id="#cartagena">CARTAGENA</a></li>
                        <li><a data-id="#floridablanca">FLORIDABLANCA</a></li>
                        <li><a data-id="#ibague">IBAGUÉ</a></li>
                        <li><a data-id="#manizales">MANIZALES</a></li>
                        <li><a data-id="#medellin">MEDELLÍN</a></li>
                        <li><a data-id="#monteria">MONTERÍA</a></li>
                        <li><a data-id="#pereira">PEREIRA</a></li>
                        <li><a data-id="#valledupar">VALLEDUPAR</a></li>
                        <li><a data-id="#villavicencio">VILLAVICENCIO</a></li>
                        <!-- <li><a data-id="#yopal">YOPAL</a></li> -->
                        
                        

                    </ul>
                </nav>

            </aside>
            <div class="col-lg-9 col-md-9 col-sm-8 tabinner concesionarios">
                <!-- Bogotá -->
                <div id="bogota" class="show contCon">
                    <div class="row capConce">
                        <div class="col-md-12">
                            <h3>Bogotá</h3>
                        </div>
                        <div class="col-md-6">
                            <strong>Mercedes-Benz Bogotá Calle 26</strong><br/>
                            <strong>Venta CV y Servicio Posventa PC y CV </strong><br/>
                            Av. Calle 26 No. 70a-25, Tel.: (1) 4236700
                        </div>
                        <div class="col-md-6">

                            <strong>Mercedes-Benz Bogotá Calle 183</strong><br/>
                            <strong>Venta y AMG Performance Center y Servicio Express PC y AMG </strong>  <br/>
                            Autopista Norte No. 183A-58, Tel.: (1) 4236700
                        </div>
                    </div>
                     <div class="row capConce">

                        <div class="col-md-6">
                            <strong>Automercol </strong><br>
                            <strong>Venta PC </strong><br> 
                            Calle 90 No. 12-31, Tel.: (1) 6162633<br>
                            Calle 116 No. 18-48, Tel.: (1) 7424140<br>
                            <strong>Servicio Posventa PC</strong><br>
                            Calle 99 No. 11-42, Tel.: (1) 3902606<br>
                            <strong>Servicio Express PC</strong><br>
                            Carrera 30 No. 64 A - 41, Tel.: (1) 4660858
                        </div>
                        <div class="col-md-6">
                            <strong>Intermarcali</strong><br>
                            <strong>Venta y Servicio Posventa PC y AMG</strong><br> 
                            Carrera. 13 No. 34-76, Tel.: (1) 3275000<br>
                            <strong>Venta PC y AMG  </strong><br>
                            Carrera 7 No. 75-06, Tel.: (1) 3275000
                        </div>
                    </div>
                     <div class="row capConce">

                        <div class="col-md-6">
                            <strong>Automotores La Floresta</strong><br>
                            <strong>Venta y Servicio Posventa PC y CV (MB y Vans)</strong><br>
                            Av. Carrera 68 No. 95-73, (1) 6439300<br>
                            <strong>Centro Chía </strong><br>
                            <strong>Venta PC </strong><br>
                            Avenida Pradilla 900 este, C.C Centro Chía, Tel.: (1) 6439300
                        </div>
                        <div class="col-md-6">
                            <strong>Motorysa Bogotá</strong><br>
                            <strong>Av. 68</strong><br>
                            <strong>Venta y Servicio Posventa CV</strong><br>
                            Av. Carrera 68 No. 68B - 61	(1) 7448844
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-6">
                            <strong>Motorysa Fuso</strong><br>
                            <strong>Venta y Servicio Posventa Fuso </strong><br>
                            Calle 13 No. 50 - 51, Tel.: (1) 7564466
                        </div>
                        
                        <div class="col-md-6">
                            <strong>Tramicon</strong><br>
                            <strong>Distribuidor de Repuestos y Taller Autorizado Camiones FL y MB  </strong><br>
                            Calle 19 No. 68-75, (1) 2609923
                        </div>   
                    </div>
                    <div class="row capConce">
                        <div class="col-md-6">
                            <strong>Impardiesel</strong><br>
                            <strong>Distribuidor de Repuestos</strong><br>
                            Avenida Centenario No. 90-70, (1) 4217963
                        </div>
                         <div class="col-md-6">
                            <strong>Concesionario Starniza</strong><br>
                            <strong>Venta PC</strong><br>
                            AVENIDA 170 # 64 - 80 , (1) 3788515
                        </div>


                        
                        
                    </div>
                    
                </div><!--/.bogota-->
                
                <!--Barranquilla-->
                <div id="barranquilla" class="contCon">
                    <div class="row capConce">
                        <div class="col-md-12">
                            <h3>Barranquilla</h3>
                        </div>
                        <div class="col-md-12">
                            <strong>Alemana Automotriz</strong><br>
                            <strong>Sala de Ventas PC </strong><br>
                            Cra. 60 No. 75-175, Tel.: (5) 3451414<br>
                            <strong>Sala de Ventas CV & Servicio Posventa PC, CV</strong><br>
                            Vía 40 No. 85B-300, Tel.: (5) 3773159
                        </div>
                    </div><!--
                     
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Motorysa Fuso</strong><br>
                            <strong>Venta y Servicio Posventa Fuso </strong><br>
                            Vía 40 No. 67 - 180, Tel.: (5) 3850222
                        </div>
                    </div>-->
                </div><!-- /.barranquilla-->
                <!--Bucaramanga-->
                <div id="bucaramanga" class="contCon">
                    <div class="row capConce">
                        <div class="col-md-12">
                            <h3>Bucaramanga</h3>
                        </div>
                        <div class="col-md-12">
                            <strong>Motoreste Motors</strong><br>
                            <strong>Sala de Ventas PC, CV & Servicio Posventa PC, CV</strong><br>
                           Autopista Floridablanca No. 91-55, Tel.: (7) 6360160
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Motorysa Fuso</strong><br>
                            <strong>Venta y Servicio Posventa Fuso </strong><br>
                            Carrera 27 No. 35-07, Tel.: (7) 6975000
                        </div>
                    </div>
                </div><!-- /.bucaramanga-->
                
                <!--Cali-->
                <div id="cali" class="contCon">
                    <div class="row capConce">
                        <div class="col-md-12">
                            <h3>Cali</h3>
                        </div>
                        <div class="col-md-6">
                            <strong>Andina Motors Carrera 8</strong><br>
                            <strong>Venta CV y Servicio Posventa PC y CV </strong><br>
                           Carrera 8 No. 33-16, Tel.: (2) 4852727
                        </div>
                        <div class="col-md-6">
                            <strong>Andina Motors Av. 6</strong><br>
                            <strong>Venta PC</strong><br>
                           Av. 6 A norte No. 20N-34, Tel.: (2) 4852727
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-6">
                            <strong>Andina Motors Ciudad Jardín</strong><br>
                            <strong>Venta PC</strong><br>
                           Calle 16 No. 103-44, Tel.: (2) 4852727
                        </div>
                        <div class="col-md-6">
                            <strong>Motorysa Fuso</strong><br>
                            <strong>Venta y Servicio Posventa Fuso </strong><br>
                            Calle 13 No. 69-24, Tel.: (2) 4869090
                        </div>
                    </div>
                </div><!-- /.cali-->
                
                <!--Cúcuta-->
                <div id="cucuta" class="contCon">
                    <div class="row capConce">
                        <div class="col-md-12">
                            <h3>Cúcuta</h3>
                        </div>
                        <div class="col-md-12">
                            <strong>Motoreste Motors</strong><br>
                            <strong>Venta CV </strong><br>
                           Avenida Libertadores 2 - 100, Tel.: 3114626076
                        </div>
                    </div>
                    
                    <div class="row capConce">
                        
                        <div class="col-md-12">
                            <strong>Orient Trucks</strong><br>
                            <strong>Distribuidor de repuestos</strong><br>
                           Avenida 3 No.8-19, Tel.: (7) 5831065
                        </div>
                    </div>
                </div><!-- /.cucuta-->
                
                <!--Cartagena-->
                <div id="cartagena" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Cartagena</h3>
                        </div>
                     </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Alemana Automotriz</strong><br>
                            <strong>Sala de Ventas PC & Express Service</strong><br>
                            Cra. 3 No. 9-148, Tel.: (5) 6552969
                        </div>
                    </div>
                    
                    
                </div><!-- /.cartagena-->
                
                <!--Ibague-->
                <div id="ibague" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Ibagué</h3>
                        </div>
                    </div>
                    
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Motorysa Ibagué</strong><br>
                            <strong>MiroLindo</strong><br>
                            <strong>Venta y Servicio Posventa PC</strong><br> 
                            Cruce deportivo Glorieta Mirolindo, (8) 2770500<br> <br>
                            
                            <strong>Picaleña</strong><br>
                            <strong>Venta y Servicio Posventa CV</strong><br> 
                            Carrera 48 sur No. 83-15 km 4 vía Picaleña, (8) 2771828
                        </div>
                    </div>
                </div><!-- /.ibague-->
                
                <!--Manizales-->
                <div id="manizales" class="contCon">
                    <div class="row ">
                        <div class="col-md-12">
                            <h3>Manizales</h3>
                        </div>
                    </div>
                    
                    
                    <div class="row capConce">
                        <div class="col-md-12">
                           <strong>Andes Motors</strong><br>
                            <strong>Venta PC</strong><br>
                            Cra. 23 No. 69A-96	, (6) 8874499
                        </div>
                    </div>
                </div><!-- /.manizales-->
                
                <!--Medellin-->
                <div id="medellin" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Medellín</h3>
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Alemautos Mall Río</strong><br>
                            <strong>Venta y Servicio Posventa PC y AMG</strong><br>
                            Calle 10 No. 50 - 243, Tel.: (4) 4442369
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Alemautos Llano Grande</strong><br>
                            <strong>Venta y AMG Performance Center y Servicio Express PC y AMG </strong><br>
                            Kilómetro 6 Vía Don Diego, Tel.: (4) 4442369
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                           <strong>Alemautos Autopista Sur</strong><br>
                            <strong> Venta y Servicio Posventa CV </strong><br>
                            Carrera 50 No. 79 Sur-30, Tel.: (4) 4442369
                        </div>
                    </div> 
                </div><!-- /.medellin-->
                
                <!--Pereira-->
                <div id="pereira" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Pereira</h3>
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Andes Motors</strong><br>
                            <strong>Uniplex</strong><br>
                            <strong>Sala de Ventas PC</strong><br>
                            Av. Circunvalar No. 13-40 Uniplex, Tel.: (6) 3247575
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                             <strong>Tierra buena</strong><br>
                            <strong>Sala de Ventas CV & Servicio Posventa PC, CV</strong><br>
                            Av. 30 de Agosto No. 103-81, Tel.: (6) 3247575
                        </div>
                    </div>
                </div><!-- /.Pereira-->
                
                <!--Villavicencio-->
                <div id="villavicencio" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Villavicencio</h3>
                        </div>
                    </div>
<!--                     <div class="row capConce">
                        <div class="col-md-12">
                            <strong>MB Llanos</strong><br>
                            <strong>Venta y Servicio Posventa CV</strong><br>
                            Calle 1 No. 36-16 Anillo Vial Tel.: (8) 6827272<br>
                            
                        </div>
                    </div>
 -->                    <div class="row capConce">
                        <div class="col-md-12">
                           <strong>Motorysa Fuso</strong><br>
                            <strong>Venta y Servicio Posventa Fuso </strong><br>
                            Avenida 40 No. 20 - 55, Tel.: (8) 6849800
                        </div>
                    </div>
                </div><!-- /.Villavicencio-->
                
                <!-- YOPAL -->
                <div id="yopal" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Yopal</h3>
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>MB Llanos</strong><br>
                            <strong>Venta y Servicio Posventa CV</strong><br>
                            Transversal 1 Cra. 3 oeste, km 1 vía Yopal – Aguazul, Tel.: (8) 6827272
                        </div>
                    </div>
                </div><!-- /.YOPAL-->
                
                 <!-- Montería -->
                <div id="monteria" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Montería</h3>
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Montería</strong><br>
                            <strong>Venta y Servicio Posventa CV </strong><br>
                            Cra. 6 No. 71 – 58 Barrio San Francisco, (4) 7822406
                        </div>
                    </div>
                </div><!-- /.MonteríaL-->
                
                <!-- Floridablanca -->
                <div id="floridablanca" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Floridablanca</h3>
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Floridablanca</strong><br>
                            <strong>Venta y Servicio Posventa PC y CV </strong><br>
                            Autopista Florida Blanca No. 91-55, (7) 6360160
                        </div>
                    </div>
                </div><!-- /.Floridablanca-->
                
                <!-- Boyacá -->
                <div id="boyaca" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Boyacá</h3>
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Motorysa Duitama</strong><br>
                            <strong>Venta CV y Servicio Posventa PC y CV </strong><br>
                           Vereda San Lorenzo Kilómetro 4 Vía Paipa - Duitama, 3223495959
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Tractocamiones de las Américas</strong><br>
                            <strong>Distribuidor de Repuestos</strong><br>
                           Autopista Central de Norte 600 Mts. Vía Duitama-Paipa, (8) 7600573
                        </div>
                    </div>
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Luciano Chaparro</strong><br>
                            <strong>Distribuidor de Repuestos</strong><br>
                           Km 1 Vía Duitama - Paipa, Sector Higueras, (8) 7602715
                        </div>
                    </div>
                    
                </div><!-- /.Boyacá-->
                <!-- Valledupar -->
                <div id="valledupar" class="contCon">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Valledupar</h3>
                        </div>
                    </div> 
                    <div class="row capConce">
                        <div class="col-md-12">
                            <strong>Alemana Automotriz</strong><br>
                            <strong>Ventas PC</strong><br>
                           Calle 30 No. 6A - 50 C.C Mayasales Plaza II local 237, Tel. (5) 5732030 
                        </div>
                    </div>
                    
                </div><!-- /.Valledupar-->
                
            </div>  
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al Inicio">Home</a><span class="sep">></span>
        <a href="concesionarios.php" title="Ir a Red de concesionarios">Red de concesionarios</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      