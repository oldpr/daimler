<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include "include/header.php";
require 'vendor/autoload.php';

error_reporting(E_ALL); // Error engine - always ON!

ini_set('display_errors', TRUE); // Error display - OFF in production env or real server

ini_set('log_errors', TRUE); // Error logging

ini_set('error_log', 'your/path/to/errors.log'); // Logging file

ini_set('log_errors_max_len', 1024); // Logging file size



$mail = new PHPMailer(true);

$message1="";


?>



<style>
    [type="checkbox"]:not(:checked),
    [type="checkbox"]:checked {
        position: absolute;
        left: -9999px;
    }
    [type="checkbox"]:not(:checked) + label,
    [type="checkbox"]:checked + label {
        position: relative;
        padding-left: 25px;
        cursor: pointer;
    }

    [type="checkbox"]:not(:checked) + label:before,
    [type="checkbox"]:checked + label:before {
        content: '';
        position: absolute;
        left:0; top: 1px;
        width: 20px; height: 20px;
        border: 1px solid #aaa;
        background: #f8f8f8;
        border-radius: 3px;
    }

    [type="checkbox"]:not(:checked) + label:after,
    [type="checkbox"]:checked + label:after {
        content: '\2713';
        position: absolute;
        top: 3px; left: 4px;
        font-size: 18px;
        line-height: 0.8;
        color: darkcyan;
        transition: all .2s;
    }
    [type="checkbox"]:not(:checked) + label:after {
        opacity: 0;
        transform: scale(0);
    }
    [type="checkbox"]:checked + label:after {
        opacity: 1;
        transform: scale(1);
    }
</style>

<script>

function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 2) {
            alert('El archivo supera los 2MB.');
            $('#hojaVida').val(''); //for clearing with Jquery
        } else {

        }
    }


</script>

<div class="inner clientes">
    <div class="tituloheader"><h1> Trabaje con nosotros</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera mercedes" style="background-image: url('img/contacto.jpg')">
    </div>

    <!--CONTENIDOS-->
    <div class="container content-interna">
        <div class="row">
            <div class="col-md-3">

                <p>
                    En Daimler buscamos gente apasionada, exigente con los objetivos que se propone, integra y comprometida con la excelencia.
                    <br>
                    <strong> ¡The best or nothing!</strong>
                </p>

            </div>
            <div class="col-md-8 ">
                <h3 class="margin0"></h3>

                <div class="col-md-10">
                    <div class="row">
                      <div style="margin-bottom: 4%;"> <center> <?php  if(isset($_GET['sent'])){
                            if($_GET['sent']==true){ $mensajeFinal='Se ha enviado su hoja de vida correctamente.'; }else{  $mensajeFinal='Ha ocurrido un error al enviar su hoja de vida, contacte al administrador.'; } echo $mensajeFinal;  }  ?> </center></div>
                        <form action="subirhojavida.php" accept-charset="utf-8" id="formContacto" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input  type="text" name="nombre" id="nombre" class="form-control input-lg" placeholder="Nombre completo" required/>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Correo electrónico" required/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="ciudad" id="ciudad" class="form-control input-lg" placeholder="Ciudad" required/>
                            </div>
                            <div class="form-group">
                                <input  type="text" name="telefono" id="telefono" class="form-control input-lg" placeholder="Teléfono" required/>
                            </div>
                            <div class="form-group">
                                <textarea name="mensaje" class="form-control input-lg" rows="3" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="form-group">
                                <input style="width: 500px;height: 30px;opacity: 0;" onchange="ValidateSize(this)" type="file" data-max-size="1" name="hojaVida" id="hojaVida" class="inputfile" required="" accept=".pdf, .txt, .doc, .docx" data-multiple-caption="{count} files selected" required/>
                                <label for="hojaVida" class="labelFile"><img src="img/icon-small-down.png" alt=""/> <span>Adjuntar hoja de vida</span></label>
                            </div>

                            <div class="form-group text-left">
                                <input type="checkbox" class="form-control" name="conf" id="conf"/>
                                <label for="conf" style='font-weight:normal;'>Acepto haber le&iacute;do y comprendido el MANUAL INTERNO DE POL&Iacute;TICAS Y DE PROCEDIMIENTOS PARA GARANTIZAR EL TRATAMIENTO DE LOS DATOS PERSONALES de DAIMLER COLOMBIA S.A. y por lo tanto, acepto libremente el tratamiento que se le dar&aacute; a mis datos transferir dichos a las personas naturales o jur&iacute;dicas de acuerdo con las finalidades y condiciones mencionadas en dicho Manual y en la Pol&iacute;tica de Protecci&oacute;n de Datos Personales</label>
                            </div>
                            <div id="check_msg" style="color:#FF0000"></div>

                            <button type="submit" name="submit" class="btn btn-negro btnContact input-lg">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>

        <a href="trabaje-con-nosotros.php" title="Ir a Trabaje con nostros">Trabaje con nosotros</a>
    </div>
</div>






<?php
include "include/destacados.php";
include "include/footer.php";
?>