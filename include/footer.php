<footer class="footer container">
                <div class="row">
                    <div class="menu">
                        <ul>
                            <li><a href="http://www.daimler.com" target="blank">Worldwide</a></li>
                            <li><a href="<?php echo $path ?>mapa-del-sitio.php">Mapa del sitio</a></li>
                            <li><a href="<?php echo $path ?>contacto.php">Contáctenos</a></li>
                            <!--<li><a href="<?php echo $path ?>ciudadanodaimler" target="blank">Ciudadano Daimler</a></li>-->

                        </ul>
                    </div>
                </div>
                <div class="logos">
                    <a href="http://www.mercedes-benz.com.co" target="blank"><img src="<?php echo $path ?>img/logo-mb.png" alt="Logo Mercedes"/></a>
                    <a href="http://www.freightliner.com.co" target="blank"><img src="<?php echo $path ?>img/freightliner.jpg" alt="Logo Freightliner"/></a>
                    <a href="http://www.fuso.com.co/" target="blank"><img src="<?php echo $path ?>img/fuso.jpg" alt="Logo Fuso"/></a>
                    <a href="http://portal.daimler.com.co/" target="blank"><img src="<?php echo $path ?>img/portal.png" alt="Logo Portal Daimler"/></a>

                </div>
                <div class="copy">&copy; 2016 Daimler Colombia All Rights Reserved    Política de privacidad</div>
                
            </footer>

        
        <script src="<?php echo $path ?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $path ?>js/jquery.bootstrap-dropdown-hover.min.js" type="text/javascript"></script>
        <script>
            
            $('.menu-principal [data-toggle="dropdown"]').bootstrapDropdownHover({
                // see next for specifications
              });
        </script>
        <script src="<?php echo $path ?>js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php echo $path ?>js/scripts.js" type="text/javascript"></script>
        <script>
            $('#formContacto').submit(function(){
                var form=1;
               $("#formContacto .form-control").each(function(){
                   if ($(this).val() == ""){
                       $(this).addClass("erro");
                      form=0;
                   }else{
                        if(form==1){
                            
                            $(this).removeClass("erro");
                            if($("#conf").is(':checked')){
                                
                            }else{
                                $("#check_msg").html("Seleccione la casilla de aceptaci&oacute;n de t&eacute;rminos y condiciones para enviar el mensaje");
                                form=0;
                            }
                        }
                   }
               });
               if(form == 0){
                   return false;
               }
            });
        </script>
    </body>
</html>
