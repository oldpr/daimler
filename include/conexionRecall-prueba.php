<?php
$dsn = 'mysql:host=localhost;dbname=name_database';
$nombre_usuario = 'user_database';
$contraseña = 'pass_database';
$opciones = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
); 

$conexion = new PDO($dsn, $nombre_usuario, $contraseña, $opciones);