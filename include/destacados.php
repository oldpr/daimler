<!-- DESTACADOS -->
<section id="destacados" class="container">
    <div class="row">
        <div class="col-md-4 col-lg-4">
          <div class="capsulanew">
              <div class="image" style="background-image: url('<?php echo $path ?>img/home/img1.jpg')"></div>
              <div class="caption">
                <div class="arrow"></div>
                <h3>Campaña de seguridad</h3>
                
              </div>
              <a href="<?php echo $path ?>recall"></a>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="capsulanew">
              <div class="image" style="background-image: url('<?php echo $path ?>img/home/img2.jpg')"></div>
              <div class="caption">
                  <div class="arrow"></div>
                <h3>Proveedores</h3>
               
              </div>
              <a href="<?php echo $path ?>condiciones-generales.php"></a>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="capsulanew">
              <div class="image" style="background-image: url('<?php echo $path ?>img/home/img3.jpg')"></div>
              <div class="caption">
                  <div class="arrow"></div>
                <h3>Nuestra Compañía</h3>
                
              </div>
              <a href="<?php echo $path ?>compania.php"></a>
          </div>

        </div>
    </div>
</section><!--/.secc2-->

