<!DOCTYPE html>
<?php 
$path = '';//"/"
if (!isset($cliente)) $cliente = '';
if (!isset($provee)) $provee = '';
?>
<html>
    <head>
        <title>Daimler - Colombia</title>
        <link rel="shortcut icon" href="http://www.daimler.com.co/favicon.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!--meta charset="UTF-8"-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $path ?>css/bootstrapStyle.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path ?>css/owl.carousel.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path ?>css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path ?>css/media.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path ?>css/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path ?>css/yamm.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $path ?>css/styleIcon.css" rel="stylesheet" type="text/css"/>
        
        <script src="<?php echo $path ?>js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $path ?>js/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="<?php echo $path ?>js/jquery.fancybox-media.js" type="text/javascript"></script>
        
        
    </head>
    <body>
 
        <header class="header">
           <div class="container">
            <div class="row">
                
                <div class="col-md-12 terminos text-right">
                    <a href="<?php echo $path ?>terminosycondiciones.php">Términos y Condiciones<i class="info"></i></a>
                </div>
                <div class="col-sm-3 col-xs-2 text-right hidden-lg hidden-md">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-mobile" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button> 
                </div>
                <div class="col-md-6 col-sm-5 col-xs-10 innerlogo ">
                    <a href="<?php echo $path ?>"><img src="<?php echo $path ?>img/logo.png" alt=""/></a>
                </div>
                <div class="col-md-6 col-sm-4 col-xs-12 name text-right innernombre">
                   Daimler Colombia
                </div>
            </div>
                 <nav class="menu-principal yamm " role="navigation">   
                <!--menu desk-->
                   <ul>
                       <li><a href="<?php echo $path ?>index.php">INICIO</a></li>
                       <li><a href="<?php echo $path ?>compania.php">COMPAÑÍA</a></li>
                       <li class="dropdown yamm-fw">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CLIENTES</a>
                           <nav class="contmenu dropdown-menu submenu ">
                                <ul class="container sbm sbm1">
                                   <li><a href="<?php echo $path ?>mercedes-benz-turismo.php" <?php if ($cliente == "mercedes"){echo ' class="text-strong"';} ?>>Mercedes-Benz Automóviles</a></li>
                                   <li><a href="<?php echo $path ?>mercedes-benz-comerciales.php" <?php if ($cliente == "mercedes-comercial"){echo ' class="text-strong"';} ?>>Mercedes-Benz Vehículos Comerciales</a></li>
                                   <li><a href="<?php echo $path ?>freightliner.php" <?php if ($cliente == "freightliner"){echo ' class="text-strong"';} ?>>Freightliner</a></li>
                                   <li><a href="<?php echo $path ?>fuso.php" <?php if ($cliente == "fuso"){echo ' class="text-strong"';} ?>>FUSO</a></li>
                                   <li><a href="<?php echo $path ?>creacion-actualizacion-clientes.php" <?php if ($cliente == "datos"){echo ' class="text-strong"';} ?>>Actualización de clientes</a></li>
                                 </ul> 
                           </nav>
                       </li>
                       <li class="dropdown yamm-fw">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROVEEDORES</a>
                           <nav class="contmenu dropdown-menu submenu sbm2">
                           <ul class="container sbm sbm2">
                              <li><a href="<?php echo $path ?>condiciones-generales.php" <?php if ($provee == "condiciones"){echo ' class="text-strong"';} ?>>Condiciones generales</a></li>
                              <li><a href="<?php echo $path ?>creacion-actualizacion-proveedores.php" <?php if ($provee == "creacion"){echo ' class="text-strong"';} ?>>Creación y/o actualización proveedores</a></li>
                            </ul>
                           </nav>
                       </li>
                       <li><a href="<?php echo $path ?>concesionarios.php">RED DE CONCESIONARIOS</a></li>
                       <li><a href="<?php echo $path ?>recall">CAMPAÑA SEGURIDAD</a></li>
                       <li><a href="<?php echo $path ?>contacto.php">CONTACTO</a></li>
                       <li><a href="<?php echo $path ?>trabaje-con-nosotros.php">TRABAJE CON NOSOTROS</a></li>
                   </ul> 
                 </nav>
            <!--menu mobile-->
                    
           </div>
        </header>
        
        <div class="collapse navbar-collapse" id="menu-mobile">
                         <ul class="navbar-nav">
                           <li><a href="<?php echo $path ?>">INICIO</a></li>
                           <li><a href="<?php echo $path ?>compania.php">COMPAÑÍA</a></li>
                           <li class="dropdown">
                               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CLIENTES</a>
                               <ul class="dropdown-menu">
                                  <li><a href="<?php echo $path ?>mercedes-benz.php">Mercedes-Benz</a></li>
                                  <li><a href="<?php echo $path ?>freightliner.php">Freightliner</a></li>
                                  <li><a href="<?php echo $path ?>fuso.php">FUSO</a></li>
                                  <li><a href="<?php echo $path ?>creacion-actualizacion-clientes.php">Creación y/o actualización clientes</a></li>
                                </ul> 
                           </li>
                           <li class="dropdown">
                               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROVEEDORES</a>
                               <ul class="dropdown-menu">
                                  <li><a href="<?php echo $path ?>condiciones-generales.php">Condiciones generales</a></li>
                                  <li><a href="<?php echo $path ?>creacion-actualizacion-proveedores.php">Creación y/o actualización proveedores</a></li>
                                </ul>
                           </li>
                           <li><a href="<?php echo $path ?>concesionarios.php">RED DE CONCESIONARIOS</a></li>
                           <li><a href="<?php echo $path ?>recall">CAMPAÑA SEGURIDAD</a></li>
                           <li><a href="<?php echo $path ?>contacto.php">CONTACTO</a></li>
                           <li><a href="<?php echo $path ?>trabaje-con-nosotros.php">TRABAJE CON NOSOTROS</a></li>
                       </ul> 
                         
                     </div>
        