
<!-- BARRA LATERAL -->
<aside class="col-lg-4 col-md-4 col-sm-4">
<?php 
    if($cliente != "freightliner"){
?>
     <div class="capsulanew">
        <div class="image" style="background-image: url('img/clientes/freightliner.jpg')"></div>
        <div class="caption">
            <div class="arrow"></div>
          <h3>Freightliner</h3>
         
        </div>
        <a href="freightliner.php"></a>
    </div>
<?php } ?>
<?php 
    if($cliente != "fuso"){
?>
  <div class="capsulanew">
      <div class="image" style="background-image: url('img/clientes/fuso.jpg')"></div>
      <div class="caption">
          <div class="arrow"></div>
        <h3>FUSO</h3>
        
      </div>
      <a href="fuso.php"></a>
  </div>
<?php } ?>
<?php 
if($cliente != "datos"){
?>
    <div class="capsulanew">
      <div class="image" style="background-image: url('img/clientes/internet.jpg')"></div>
      <div class="caption">
          <div class="arrow" style="background-position: -33px 4px; height: 61px; top:1px; right: -5px;"></div>
        <h3 class="min">Actualización de datos</h3>
        
      </div>
      <a href="creacion-actualizacion-clientes.php"></a>
  </div>
 <?php } ?>
<?php 
if($cliente != "mercedes"){
?>
    <div class="capsulanew">
      <div class="image" style="background-image: url('img/header_MB.jpg')"></div>
      <div class="caption">
          <div class="arrow"></div>
        <h3 class="min">Mercedes-Benz</h3>
        
      </div>
      <a href="mercedes-benz-turismo.php"></a>
  </div>
<?php } ?>

<?php
if($cliente != "mercedes-comercial"){
?>
    <div class="capsulanew">
      <div class="image" style="background-image: url('img/header_MB-comerciales.jpg')"></div>
      <div class="caption">
          <div class="arrow"></div>
        <h3 class="min">Mercedes-Benz Vehículos Comerciales</h3>
        
      </div>
      <a href="mercedes-benz-comerciales.php"></a>
  </div>
<?php } ?>

</aside>