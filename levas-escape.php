<?php 
include "../include/header.php";
$active="escape";
?>
<div class="inner clientes">
    <?php include "../include/banner_recall.php";?>
    
    <div class="container content-interna interna-campana">
        <div class="row">
            <?php 
                include "../include/menu_recall.php";
            ?>
            <div class="col-lg-8 col-md-8 col-sm-9">
                <img src="../img/recall/clasea.jpg" alt=""/>
                <p class="small">*Fotografías de Referencia</p>
                <h3>Árbol de levas escape Clase A, Clase B, Clase CLA, Clase GLA, Clase C y Clase E. </h3>
                <h3>Año/modelo 2016, equipados con motor a gasolina</h3>
                
                <p>Consulte con el número de chasis de su vehículo, si éste requiere renovación del árbol de levas de escape del motor. Lo anterior debido a que es posible que la costura soldada en uno de los lotes no se haya ejecutado correctamente en los vehículos afectados. </p>
                <p><strong>NOTA: Hacer caso omiso a esta campaña de seguridad puede generar daños permanentes en el motor y en consecuencia un alto riesgo de accidente.</strong> 
                    <form class="valid">
                        <input required type="text" class="form-control input-lg validar" id="chasis" placeholder="Ingrese el número de su chasis">
                        <input type="hidden" id="tipo" value="levas">
                        <button type="submit" class="btn btn-negro input-lg validarchasis" >Validar chasis</button>
                    </form>
                
                <p class="texto-inicio">Los números de chasis se componen de 17 dígitos y se encuentran en la tarjeta de propiedad del vehículo.</p>
                
                <div class="msj msjconfirma">
                <p>El número de chasis <span class="nchasis">3ALHC5CV7FDGN5231</span> aplica  para la campaña de renovación del árbol de levas de escape del motor. </p>
                <p>Por favor acérquese a su concesionario autorizado Mercedes-Benz más cercano.</p>
<a href="../concesionarios.php" class="btn btn-negro">RED DE CONCESIONARIOS</a>
                </div>
                
                <div class="msj msjrechaza ">
                <p>El número de chasis <span class="nchasis">3ALHC5CV7FDGN5231</span> no aplica para campaña.</p>
                
                </div>
            </div>  
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
   <div class="row">
        <a href="<?php echo $path ?>index.php" title="Ir al Inicio">Home</a><span class="sep">></span>
        <a href="index.php">Campaña de seguridad</a> <span class="sep">></span>
        <a href="<?php echo $path ?>levas-escape.php" title="Ir a Arbol de levas escape ">Arbol de levas escape </a> 
    </div>
</div>
 <script src="recallBackend/recall.js" type="text/javascript"></script>
<?php 
include "../include/destacados.php";
include "../include/footer.php";
?>      